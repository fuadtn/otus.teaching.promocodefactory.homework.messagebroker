﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.MQ.Contracts;
using System.Threading;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.MQ
{
    public class ReceivedPromoCodeHostedService : BackgroundService
    {
        private readonly IConsumer<PromoCodeContract> _consumer;

        public ReceivedPromoCodeHostedService(IConsumer<PromoCodeContract> consumer) : base()
        {
            this._consumer = consumer;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }
    }
}
